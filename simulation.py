from numpy.linalg import norm
from interface import *
from programme import programme


def projection(vect1, vect2):
    scalaire = vect1[0] * vect2[0] + vect1[1] * vect2[1]
    vect = [0, 0]
    vect[0] = vect1[0] * scalaire / norm(vect2)
    vect[1] = vect1[1] * scalaire / norm(vect2)
    return vect


def main():
    liste = []
    for i in range(4):
        for j in range(3):
            liste.append([i * 3.5 * RAYON_PRISE_AU_SOL + 5, j * 3.5 * RAYON_PRISE_AU_SOL + 5])

    arene = Arene(liste, [200, 200])
    fen, dess, barre = init_affichage(arene)
    actualiser_affichage(dess, arene, barre)
    une_seconde(fen, dess, arene, barre)
    fen.mainloop()


def une_seconde(fen, dess, arene, barre):  # penser à reinitialiser les actions
    arene.temps += 1
    for i in arene.liste_robots:
        programme(arene, i)   # dans actualiser il faut prevoir les chocs
        arene.score += compter_le_score(arene)
        i.action = True

    actualiser_affichage(dess, arene, barre)
    # time.sleep(PAUSE)
    # print("temps : ",arene.temps)

    if arene.score >= SCORE_CIBLE:
        print(f"score : {arene.score}  temps : {arene.temps}")
        return arene.temps
    dess.after(int(1000 * PAUSE), une_seconde, fen, dess, arene, barre)




def compter_le_score(arene):  # penser à envoyer un message de l'attaque
    new_score = 0
    for i in arene.liste_robots:
        distance = norm([i.coord[0] - arene.cible[0], i.coord[1] - arene.cible[1]])
        if distance <= RAYON_CIBLE:
            new_score += 1
            i.message.append(["Cible", arene.cible])

    return new_score


if __name__ == "__main__":
    main()
