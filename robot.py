from constantes import *
from numpy.linalg import norm

class Robot:
    def __init__(self, coord, id):
        self.id = id
        self.coord = coord
        self.mode = "recherche"
        self.couleur = "skyblue"
        self.message = []
        self.stockage = []
        self.choc = False
        self.action = True

    def avancer(self, arene, vecteur):
        if self.action:
            # verification de la norme est bien inferieur à 1
            norme = norm(vecteur)
            if norme > 1:
                vecteur[0] *= 1 / norme
                vecteur[1] *= 1 / norme
            new_coord = [self.coord[0] + vecteur[0], self.coord[1] + vecteur[1]]
            # verification de la colision avec les murs
            # sur x
            if new_coord[0] > DIMENSIONS[0]:
                new_coord[0] = DIMENSIONS[0]
                self.choc = True
            elif new_coord[0] < 0:
                new_coord[0] = 0
                self.choc = True
            else:
                self.choc = False
            # sur y
            if new_coord[1] > DIMENSIONS[1]:
                new_coord[1] = DIMENSIONS[1]
                self.choc = True
            elif new_coord[1] < 0:
                new_coord[1] = 0
                self.choc = True

            for i in arene.liste_robots:
                if i.id != self.id:
                    distance = norm([i.coord[0] - new_coord[0], i.coord[1] - new_coord[1]])
                    if distance < RAYON_PRISE_AU_SOL:
                        new_coord = self.coord
                        self.choc = True
                        break
            self.coord = new_coord
            self.action = False

    def mouvement_est_possible(self, arene, vecteur):
        # verification de la norme est bien inferieur à 1
        norme = norm(vecteur)
        if norme > 1:
            vecteur[0] /= norme
            vecteur[1] /= norme
        new_coord = [self.coord[0] + vecteur[0], self.coord[1] + vecteur[1]]

        save_coord = [self.coord[0] + vecteur[0], self.coord[1] + vecteur[1]]

        # verification de la colision avec les murs
        # sur x
        if new_coord[0] > DIMENSIONS[0]:
            new_coord[0] = DIMENSIONS[0]
        elif new_coord[0] < 0:
            new_coord[0] = 0

        # sur y
        if new_coord[1] > DIMENSIONS[1]:
            new_coord[1] = DIMENSIONS[1]
            self.choc = True
        elif new_coord[1] < 0:
            new_coord[1] = 0

        for i in arene.liste_robots:
            if i.id != self.id:
                distance = norm([i.coord[0] - new_coord[0], i.coord[1] - new_coord[1]])
                if distance < RAYON_PRISE_AU_SOL:
                    new_coord = self.coord

                    break
        return save_coord == new_coord

    def coord_actuel(self):  # on peut faire mieux
        return self.coord

    def envoyer_message(self, arene, info):
        c = 0
        for i in arene.liste_robots:
            if i.id != self.id:
                distance = norm([i.coord[0] - self.coord[0], i.coord[1] - self.coord[1]])
                if distance < RAYON_ACTION:
                    i.message.append(info)
                    c += 1
        return c  # penser qu'il y a un retour

    def lire_messages(self):
        return self.message

    def effacer_messages(self):
        self.message = []

    def stocker(self, info):
        self.stockage = info

    def lire_stockage(self):
        return self.stockage

    def passer_en_mode(self, mode):
        self.mode = mode

    def lire_mode(self):
        return self.mode
