from tkinter import *
from tkinter import ttk

from arene import Arene
from constantes import *


def init_affichage(arene):
    fen = Tk()
    fen.title('Robotarium')
    fen.minsize(width=800, height=800)
    fen.resizable(width=YES, height=YES)

    barre = ttk.Progressbar(orient="vertical", length=500)
    barre.place(x=750, y=10)

    dess = Canvas(fen, width=700, height=700, bg='white', bd=5, relief='ridge')
    dess.place(x=10, y=10)  # prevoir de faire un affcihage variable avec une variable globale
    # prevoir aussir que dess prenne la forme de l'arenne mais avec la taille max dans la fenetre

    arene.dessin_cible.append(dess.create_oval((arene.cible[0] - RAYON_CIBLE) * 700 / DIMENSIONS[0],
                                               (arene.cible[1] - RAYON_CIBLE) * 700 / DIMENSIONS[1],
                                               (arene.cible[0] + RAYON_CIBLE) * 700 / DIMENSIONS[0],
                                               (arene.cible[1] + RAYON_CIBLE) * 700 / DIMENSIONS[1],
                                               fill='red'))  # on peut faire mieux ici

    for robot in arene.liste_robots:
        arene.dessin_rayon.append(dess.create_oval((robot.coord[0] - RAYON_ACTION) * 700 / DIMENSIONS[0],
                                                   (robot.coord[1] - RAYON_ACTION) * 700 / DIMENSIONS[1],
                                                   (robot.coord[0] + RAYON_ACTION) * 700 / DIMENSIONS[0],
                                                   (robot.coord[1] + RAYON_ACTION) * 700 / DIMENSIONS[1],
                                                   fill=robot.couleur))  # on peut faire mieux ici
        arene.dessin_robot.append(dess.create_oval((robot.coord[0] - RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[0],
                                                   (robot.coord[1] - RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[1],
                                                   (robot.coord[0] + RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[0],
                                                   (robot.coord[1] + RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[1],
                                                   fill='black'))  # on peut faire mieux ici

    return (fen, dess, barre)


def actualiser_affichage(dess: Canvas, arene: Arene, barre: ttk.Progressbar):
    for i in range(len(arene.dessin_robot)):
        dess.coords(arene.dessin_rayon[i], (arene.liste_robots[i].coord[0] - RAYON_ACTION) * 700 / DIMENSIONS[0],
                    (arene.liste_robots[i].coord[1] - RAYON_ACTION) * 700 / DIMENSIONS[1],
                    (arene.liste_robots[i].coord[0] + RAYON_ACTION) * 700 / DIMENSIONS[0],
                    (arene.liste_robots[i].coord[1] + RAYON_ACTION) * 700 / DIMENSIONS[1])
        dess.itemconfig(arene.dessin_rayon[i], fill=arene.liste_robots[i].couleur)
        dess.coords(arene.dessin_robot[i],
                    (arene.liste_robots[i].coord[0] - RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[0],
                    (arene.liste_robots[i].coord[1] - RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[1],
                    (arene.liste_robots[i].coord[0] + RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[0],
                    (arene.liste_robots[i].coord[1] + RAYON_PRISE_AU_SOL) * 700 / DIMENSIONS[1])

    barre["value"] = 100 - arene.score / SCORE_CIBLE * 100
