import random


def programme(arene, robot):
    """
    Programme embarqué dans chaque robot
    """
    if robot.lire_mode() == "recherche":
        if not robot.lire_stockage():
            x = (random.random() * 2 - 1)
            y = (random.random() * 2 - 1)  # on peut aller plus vite
            robot.stocker([x, y])

        vecteur = robot.lire_stockage()
        if robot.mouvement_est_possible(arene, vecteur):
            robot.avancer(arene, vecteur)

        else:
            x = (random.random() * 2 - 1)
            y = (random.random() * 2 - 1)  ##on peut aller plus vite
            robot.stocker([x, y])

        if len(robot.lire_messages()) > 0:
            if 'Cible' in robot.lire_messages()[0]:
                robot.stocker([robot.lire_stockage(), robot.lire_messages()[0][-1]])
                robot.passer_en_mode("messager")
                robot.couleur = "green"

            elif 'go' in robot.lire_messages()[0]:
                robot.stocker(robot.lire_messages()[0][-1])
                robot.passer_en_mode("attaque")
                robot.couleur = "red"
            robot.effacer_messages()

    if robot.lire_mode() == "messager":
        # print(lire_stockage(robot))
        vecteur = robot.lire_stockage()[0]
        if robot.mouvement_est_possible(arene, vecteur):
            robot.avancer(arene, vecteur)
        else:
            x = (random.random() * 2 - 1)
            y = (random.random() * 2 - 1)  ##on peut aller plus vite
            robot.stocker([[x, y], robot.lire_stockage()[-1]])

        robot.effacer_messages()
        # print("____",lire_stockage(robot))
        robot.envoyer_message(arene, ['go', robot.lire_stockage()[-1]])

    if robot.lire_mode() == "attaque":
        # print([lire_stockage(robot)[0]-coord_actuel(robot)[0],lire_stockage(robot)[1]-coord_actuel(robot)[1]])
        vecteur = [robot.lire_stockage()[0] - robot.coord_actuel()[0], robot.lire_stockage()[1] - robot.coord_actuel()[1]]
        robot.avancer(arene, vecteur)
        robot.effacer_messages()

