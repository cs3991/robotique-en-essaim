from robot import Robot

class Arene:
    def __init__(self, liste_coord, coord_cible):
        self.score = 0
        self.temps = 0
        self.liste_robots = []
        for i in range(len(liste_coord)):
            self.liste_robots.append(Robot(liste_coord[i], i))
        self.cible = coord_cible
        self.dessin_robot = []
        self.dessin_rayon = []
        self.dessin_cible = []
